#!/usr/bin/env python3

# Set up django
import django
django.setup()

if __name__ == '__main__':
    from gui.models import MembershipPeriod

    period = MembershipPeriod.current_period()
    if period is not None:
        for member in period.members:
            emailaddress = member.user_profile.user.emailaddress_set.filter(primary=True).first()
            if emailaddress is not None:
                print(emailaddress.email)
