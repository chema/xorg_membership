# Xorg Membership #

## Quick setup (for developers) ##

Here is a quick guide as to how to setup this website for developement:

    # Set all the parameters
    $ cp setup-env.sh.sample setup-env.sh
    $ $EDITOR setup-env.sh  # Make sure to change at least the Secret Key

    # Create a python virtual environment where we'll install all the deps
    $ python3 -m venv .
    $ source setup-env.sh
    $ pip install -r requirements.txt

    # Prepare the database
    $ python3 manage.py migrate

    # Create a super user, to be able to access the admin
    $ python3 manage.py createsuperuser

The website is now ready to be launched:

    $ source setup-env.sh
    $ python3 manage.py runserver
    $ xdg-open http://127.0.0.1:8000

Enjoy!


## Deploying the website in production

The recommended way to deploy this website is to use the docker image found in
our registry: registry.freedesktop.org/xorgfoundation/xorg_membership/main:latest

This image contains:

  * all the dependencies to deploy the latest version of the website
  * uwsgi and nginx: run the website and expose it through the wanted port (80
    by default)

### Set up

Make sure your machine is ready to execute docker images, then execute the
following commands:

    $ git clone https://gitlab.freedesktop.org/xorgfoundation/xorg_membership.git
    $ cp setup-env.sh.sample setup-env.sh
    $ $EDITOR setup-env.sh  # WARNING: Make sure to change at least SECRET_KEY!!

### Updating the website

Updating the website is as easy as downloading the latest version from git:

    $ git fetch && git reset --hard origin/master  # Safest way to update

The wsgi script will take care of migrations and collecting static files for
you, no need to worry about it!

### Starting the website

#### Simple deployment

To simplify the deployment and make it possible to influence the start-up
sequence, we provide a simple script that allows you to start the service:

    $ ./start.sh

By default, it will export the service on port 80. However, if you want to use
this service behind another web server, you might want to use a different port:

    $ ./start.sh 8080

#### Recommended deployment for production

For actual production, you might want to use the following systemd unit:

/etc/systemd/system/xorgmembership.service:

    [Unit]
    Description=X.org membership docker image
    After=network.target

    [Service]
    Type=simple
    ExecStartPre=/usr/bin/docker pull registry.freedesktop.org/xorgfoundation/xorg_membership:latest
    ExecStart=/usr/bin/docker run --rm -v /path/to/db.sqlite3:/app/db.sqlite3 --env-file /path/to/env-file -p 8080:80 --name xorg_membership registry.freedesktop.org/xorgfoundation/xorg_membership:latest
    PrivateDevices=yes

    [Install]
    WantedBy=multi-user.target

/path/to/env-file:

    SECRET_KEY=my secret key

    HOST_URL=127.0.0.1
    USE_FORWARDED_HEADERS=1

    PRODUCTION=1

    # Email server settings
    EMAIL_USE_TLS=1
    EMAIL_HOST=smtp.server.com
    #EMAIL_HOST_USER=username
    #EMAIL_HOST_PASSWORD=password
    EMAIL_PORT=587
    EMAIL_FROM=admin@isp.com

You can then just enable the systemd unit and start the service.

### Creating the initial admin user

When the website is running, you will need to create the initial user:

    $ docker exec -ti xorg_membership python3 /app/manage.py createsuperuser

You can now log in the website and do all the administrative tasks there.

### Getting the list of members

To get the current list of members, simply run the following command, which will
output one email address per line:

    $ ./get_members.sh
    email1@provider.com
    email2@provider.com
    email3@provider.com
    email4@provider.com

If the service is already running, then you can also avoid creating a new docker
instance by running:

    $ docker exec -ti xorg_membership python3 /app/get_members.py


## Contributing ##

We are welcoming contributions from anyone, and are expecting them through
Merge Requests, provided all tests pass.

To do so, please use detox before submiting your code and make it is all green:

    $ pip install detox
    $ detox
      ....
      congratulations :)

The reason we mandate this is because we value maintainability above all. To
this end, this website has been designed with testability in mind and we
mandate all tests to pass at all time on major versions of python3, along with
having 100% branch coverage for both code and templates.
