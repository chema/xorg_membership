#!/bin/bash

# Set the CWD to this script's directory
cd "$(dirname "$(readlink -f "$0")")"

source "$PWD/docker_utils.sh"

export HOST_PORT=${1:-80}
start_docker
