from django import forms
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from .models import Membership, MembershipPeriod, Profile
from .models import BallotSelectQuestionPossibleAnswer, BallotRankingQuestionOption
from .models import BallotRankingAnswer, BallotSelectedAnswer


# Origin: https://stackoverflow.com/a/32791625
class ListTextWidget(forms.TextInput):
    def __init__(self, data_list, name, *args, **kwargs):
        super(ListTextWidget, self).__init__(*args, **kwargs)
        self._name = name
        self._list = data_list
        self.attrs.update({'list': 'list__%s' % self._name})

    def render(self, name, value, attrs=None, renderer=None):
        text_html = super(ListTextWidget, self).render(name, value, attrs=attrs)
        data_list = '<datalist id="list__%s">' % self._name
        for item in self._list:
            data_list += '<option value="%s">' % item
        data_list += '</datalist>'

        return (text_html + data_list)


class ProfileForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)

    employer = forms.CharField(label='Employer', help_text="Leave empty for hobbyists",
                               max_length=80, required=False)
    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="This information will be shown only to other members and admins")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Override the widget now to ensure the list is fetched every time this object is created
        self.fields['employer'].widget = ListTextWidget(data_list=Profile.existing_employers(),
                                                        name='employers-list')

    def save(self, user):
        if self.is_valid():
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.profile.employer = self.cleaned_data['employer']
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()


class MembershipApplicationForm(forms.Form):
    period_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    user_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="Update your profile's public statement, which will be "
                                                 "used by administrators to accept/deny your application "
                                                 "and will be seen by other members")

    agree_membership = forms.BooleanField(label="I have read and agreed with the membership agreement", required=True)

    def save(self):
        if self.is_valid() and self.cleaned_data['agree_membership'] is True:
            user = get_object_or_404(User, pk=self.cleaned_data['user_id'])
            period = get_object_or_404(MembershipPeriod, pk=self.cleaned_data['period_id'])

            # Update the user profile
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()

            return Membership.objects.create(period=period, user_profile=user.profile)
        else:
            return None


class OptionRankingSubField(forms.Select):
    template_name = 'gui/widgets/option_ranking_subfield.html'

    def __init__(self, rank, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rank = rank

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['rank'] = self.rank
        return context


class OptionRankingWidget(forms.MultiWidget):
    def __init__(self, attr=None):
        super().__init__([], attr)

        # WARNING: the widgets need are initialized in the OptionRankingField

    def decompress(self, value):
        if value:
            return value
        return [None] * len(self.widgets)


class OptionRankingField(forms.MultiValueField):
    widget = OptionRankingWidget

    def __init__(self, ranking, *args, **kwargs):
        options = [(o.id, o.option) for o in ranking.possible_options.all()]
        select_choices = [('', '-- Select an option --')] + options

        fields = []
        for i, option in enumerate(options):
            fields.append(forms.ChoiceField(required=True, choices=options, label=str(i)))
        super().__init__(fields, *args, **kwargs)

        # HACK: Now create the widgets with the right options
        for i, option in enumerate(options):
            self.widget.widgets.append(OptionRankingSubField(rank=i+1, choices=select_choices))

    def compress(self, data_list):
        return data_list


class BallotForm(forms.Form):
    def __init__(self, *args, **kwargs):
        try:
            self.ballot = kwargs.pop('ballot')
        except KeyError:
            self.ballot = None

        super().__init__(*args, **kwargs)

        if self.ballot is None:
            return

        self.ranking_questions = dict()
        for i, ranking in enumerate(self.ballot.ranking_questions.all()):
            ranking_field = OptionRankingField(ranking, required=True,
                                               label=ranking.question)
            field_name = "ranking_{}".format(i)
            self.fields[field_name] = ranking_field
            self.ranking_questions[ranking] = field_name

        # Construct the form based on the questions found in the ballot. Start with the select questions
        self.select_questions = dict()
        for i, select in enumerate(self.ballot.select_questions.all()):
            answers = [('', '-- Select an answer --')] + [(a.id, a.answer) for a in select.possible_answers.all()]
            select_field = forms.ChoiceField(required=True, choices=answers,
                                             label=select.question)
            field_name = "select_{}".format(i)
            self.fields[field_name] = select_field
            self.select_questions[select] = field_name

    def _get_objects(self, name, Model, ids, fail_on_duplicates=True):
        try:
            ids = [int(id) for id in ids]
        except ValueError:
            self.add_error(name, "The selected ids are not integer")
            return None

        if fail_on_duplicates and len(set(ids)) != len(ids):
            self.add_error(name, "Options cannot be duplicated")
            return None

        objects = Model.objects.filter(pk__in=ids)
        if len(objects) != len(set(ids)):
            self.add_error(name, "At least one selected item is invalid")
            return None

        return objects

    def clean(self):
        cleaned_data = super().clean()

        for name in list(cleaned_data.keys()):
            if name.startswith('select_'):
                cleaned_data[name] = self._get_objects(name, BallotSelectQuestionPossibleAnswer, cleaned_data[name])[0]
            elif name.startswith('ranking_'):
                cleaned_data[name] = self._get_objects(name, BallotRankingQuestionOption, cleaned_data[name])
            else:
                # Not a possible case since the super().clean already scrubbed the fieldnames
                pass  # pragma: no cover

        return cleaned_data

    @transaction.atomic
    def save(self, member):
        if not self.is_valid():
            return False

        # Create all the ranking answers
        for question, field in self.ranking_questions.items():
            for rank, option in enumerate(self.cleaned_data[field]):
                BallotRankingAnswer.objects.create(question=question, member=member, option=option, rank=rank + 1)

        # Create all the select answers
        for question, field in self.select_questions.items():
            answer = self.cleaned_data[field]
            BallotSelectedAnswer.objects.create(question=question, member=member, answer=answer)

        # Finally, say that the member has voted!
        self.ballot.voters.add(member)

        return True
