function handleSelectAll() {
    /* Add a checkbox */
    $('[data-select-name]').each(function(){
        $(this).prepend('<span class="select-item"><input type="checkbox" name="' + $(this).data("select-name") + '"/></span>');
    });

    $('a[data-select-all]').click(function() {
        $("#" + $(this).data("select-all")).find("input").prop('checked', true);
        return false;
    });

    $('a[data-unselect-all]').click(function() {
        $("#" + $(this).data("unselect-all")).find("input").prop('checked', false);
        return false;
    });
}

/* Handle all the start-up scripts */
$(document).ready(function() {
    handleSelectAll();
})
