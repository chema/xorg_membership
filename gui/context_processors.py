from .models import MembershipPeriod, Membership


def admin_tasks_count(request):
    user = request.user

    admin_tasks_count = 0
    if user.is_superuser:
        admin_tasks_count = Membership.pending_memberships().count()

    return {"admin_tasks_count": admin_tasks_count}


def current_period(request):
    return {"current_membership_period": MembershipPeriod.current_period()}
