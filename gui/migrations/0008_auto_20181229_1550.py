# Generated by Django 2.1.1 on 2018-12-29 15:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gui', '0007_auto_20180929_1039'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ballot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_name', models.CharField(help_text='Short name to describe the ballot', max_length=100, unique=True)),
                ('description', models.CharField(blank=True, help_text='Longer description of the ballot containing all the relevant information for members to use to vote.', max_length=255, null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('opening_on', models.DateTimeField()),
                ('closing_on', models.DateTimeField()),
                ('voters', models.ManyToManyField(help_text='List of members who voted on this ballot', to='gui.Membership')),
            ],
        ),
        migrations.CreateModel(
            name='BallotRankingAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rank', models.PositiveSmallIntegerField(default=0, help_text='Rank this option got')),
                ('member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gui.Membership')),
            ],
        ),
        migrations.CreateModel(
            name='BallotRankingQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(blank=True, help_text='Question asking members to rank multiple options', max_length=255, null=True)),
                ('ballot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ranking_questions', to='gui.Ballot')),
            ],
        ),
        migrations.CreateModel(
            name='BallotRankingQuestionOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('option', models.CharField(help_text='One option to be ranked', max_length=100)),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='possible_options', to='gui.BallotRankingQuestion')),
            ],
        ),
        migrations.CreateModel(
            name='BallotSelectedAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='BallotSelectQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(blank=True, help_text='Question asking members to select one in multiple answers', max_length=255, null=True)),
                ('ballot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='select_questions', to='gui.Ballot')),
            ],
        ),
        migrations.CreateModel(
            name='BallotSelectQuestionPossibleAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.CharField(help_text='One possible answer', max_length=100)),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='possible_answers', to='gui.BallotSelectQuestion')),
            ],
        ),
        migrations.AddField(
            model_name='ballotselectedanswer',
            name='answer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='gui.BallotSelectQuestionPossibleAnswer'),
        ),
        migrations.AddField(
            model_name='ballotselectedanswer',
            name='member',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gui.Membership'),
        ),
        migrations.AddField(
            model_name='ballotselectedanswer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gui.BallotSelectQuestion'),
        ),
        migrations.AddField(
            model_name='ballotrankinganswer',
            name='option',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gui.BallotRankingQuestionOption'),
        ),
        migrations.AddField(
            model_name='ballotrankinganswer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gui.BallotRankingQuestion'),
        ),
        migrations.AlterUniqueTogether(
            name='ballotselectedanswer',
            unique_together={('question', 'member')},
        ),
        migrations.AlterUniqueTogether(
            name='ballotrankinganswer',
            unique_together={('question', 'member', 'option')},
        ),
    ]
