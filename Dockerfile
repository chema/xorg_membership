FROM tiangolo/uwsgi-nginx:python3.6-alpine3.8

LABEL maintainer="Martin Peres <martin.peres@free.fr>"

# Copy the website to the /app folder
COPY . /app/

# Install all our dependencies
WORKDIR /app
RUN pip install -r requirements.txt
